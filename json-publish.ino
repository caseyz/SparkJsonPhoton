// This #include statement was automatically added by the Particle IDE.
#include "SparkJson/SparkJson.h"

String value = "0";                         //Initialize reporting value
int wait = 1000;                           //Time between loops
String myID;                                //Variable for the Photon device ID
String strMqtt;                             //Variable to contain the MQTT string
int str_len;                                //Variable for String Length
                //Time Between reports

int led = D7;                               //Which LED to blink

void setup() 
    {
        Serial.begin(9600);

        pinMode(led, OUTPUT);
    } 


void loop() 
    {
        delay(wait);

        //Value data type conversion ====================================================
        //Convert the int value to a string
        String stringData;
        stringData = String("String Data");
    
        //Convert the string to Char for MQTT
        //Get the length of the string
        int str_len;
        str_len = stringData.length() + 1; 
        
        //Prepare the character array (the buffer) 
        char char_array[str_len];
        
        // Copy it over 
        stringData.toCharArray(char_array, str_len);
        
        //END Value data conversion =====================================================

        //Build json REPORT object
        
        StaticJsonBuffer<200> jsonBuffer;
        char bufferReport [200];
        
        JsonObject& root = jsonBuffer.createObject();
        root["report"] = char_array;

        Particle.publish("JSON", root.printTo(), 60, PRIVATE);
        
        root.printTo(Serial);
        root.printTo(bufferReport, sizeof(bufferReport));
        
    }


